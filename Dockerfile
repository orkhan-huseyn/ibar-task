FROM node:alpine as build
WORKDIR /app

COPY package.json ./

COPY ./client/package*.json ./client/
RUN yarn install-client-dependencies

COPY ./server/package*.json ./server/
RUN yarn install-server-dependencies

COPY . .
RUN yarn build-client

CMD [ "yarn", "run-server" ]
EXPOSE 80