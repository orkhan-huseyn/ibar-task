const { getDbPool } = require('./database');

/**
 * Get all users for select
 * @returns {Promise}
 */
async function getAllUsers() {
  return new Promise((resolve, reject) => {
    let sql = 'SELECT id, full_name as fullName FROM users';
    getDbPool().query(sql, [], (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result);
      }
    });
  });
}

/**
 * Get, paginate and filter users
 * @param {object} params
 * @returns {Promise}
 */
async function getUsers({ page, limit, fullName }) {
  return new Promise((resolve, reject) => {
    const params = [];
    let sql = 'SELECT id, full_name as fullName FROM users WHERE 1 ';
    let countSql = 'SELECT COUNT(id) as total FROM users WHERE 1 ';

    if (fullName) {
      sql += 'AND LOWER(full_name) like ? ';
      countSql += 'AND LOWER(full_name) like ? ';
      params.push(`%${fullName.toLowerCase()}%`);
    }

    sql += 'LIMIT ? OFFSET ?';
    params.push(limit, (page - 1) * limit);

    getDbPool().query(
      `${sql}; ${countSql}`,
      [...params, ...params],
      (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve([result[0], result[1][0].total]);
        }
      }
    );
  });
}

/**
 * Get single user by id
 * @param {number} id
 */
async function getUserById(id) {
  return new Promise((resolve, reject) => {
    const sql = 'SELECT id, full_name as fullName FROM users  WHERE id=?';
    getDbPool().query(sql, [id], (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result[0]);
      }
    });
  });
}

/**
 * Create new user in db
 * @param {object} fields
 */
async function createUser({ fullName }) {
  return new Promise((resolve, reject) => {
    const sql = 'INSERT INTO users (full_name) VALUES ?';
    const values = [[fullName]];
    getDbPool().query(sql, [values], (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result.insertId);
      }
    });
  });
}

/**
 * Update user in db
 * @param {object} fields
 */
async function updateUser({ id, fullName }) {
  return new Promise((resolve, reject) => {
    const sql = 'UPDATE users SET full_name=? WHERE id=?';
    getDbPool().query(sql, [fullName, id], (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result.affectedRows);
      }
    });
  });
}
/**
 * Delete user from db
 * @param {number} id
 */
async function deleteUser(id) {
  return new Promise((resolve, reject) => {
    getDbPool().query(
      'DELETE FROM posts WHERE user_id=?',
      [id],
      (err, result) => {
        //TODO: what to do?
      }
    );

    const sql = 'DELETE FROM users WHERE id=?';
    getDbPool().query(sql, [id], (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result.affectedRows);
      }
    });
  });
}

module.exports = {
  getAllUsers,
  getUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser,
};
