const path = require('path');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const {
  getPosts,
  getPostById,
  createPost,
  updatePost,
  deletePost,
} = require('./posts');

const {
  getAllUsers,
  getUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser,
} = require('./users');

const app = express();
const port = process.env.PORT || 80;

app.use(cors());
app.use(bodyParser.json());
app.use(express.static(path.resolve(__dirname, '../client/build')));

app.listen(port, () => {
  console.log(`REST API listening on port ${port}`);
});

/** paginate and filter users */
app.get('/api/v1/users', async (req, res) => {
  const page = req.query.page ? parseInt(req.query.page, 10) : 1;
  const limit = req.query.limit ? parseInt(req.query.limit, 10) : 10;
  const fullName = req.query.fullName;

  const [users, total] = await getUsers({ page, limit, fullName });
  res.status(200).json({ users, total: total });
});

/** get all users for select */
app.get('/api/v1/users/all', async (req, res) => {
  const users = await getAllUsers();
  res.status(200).json(users);
});

/** get user by id */
app.get('/api/v1/users/:id', async (req, res) => {
  const user = await getUserById(req.params.id);
  res.status(200).json(user);
});

/** create new user */
app.post('/api/v1/users', async (req, res) => {
  const id = await createUser(req.body);
  const user = await getUserById(id);
  res.status(201).json(user);
});

/** update existing user */
app.put('/api/v1/users/:id', async (req, res) => {
  const affectedRows = await updateUser({ id: req.params.id, ...req.body });
  console.log(affectedRows);
  const user = await getUserById(req.params.id);
  res.status(200).json(user);
});

/** delete existing user */
app.delete('/api/v1/users/:id', async (req, res) => {
  const affectedRows = await deleteUser(req.params.id);
  console.log(affectedRows);
  res.status(204).json(null);
});

/** get, filter and paginate posts */
app.get('/api/v1/posts', async (req, res) => {
  const page = req.query.page ? parseInt(req.query.page, 10) : 1;
  const limit = req.query.limit ? parseInt(req.query.limit, 10) : 10;
  const fullName = req.query.fullName;
  const title = req.query.title;
  const content = req.query.content;

  const [posts, total] = await getPosts({
    page,
    limit,
    fullName,
    title,
    content,
  });
  res.status(200).json({ posts, total: total });
});

/** get post by id */
app.get('/api/v1/posts/:id', async (req, res) => {
  const post = await getPostById(req.params.id);
  res.status(200).json(post);
});

/** create new post */
app.post('/api/v1/posts', async (req, res) => {
  const id = await createPost(req.body);
  const post = await getPostById(id);
  res.status(201).json(post);
});

/** update single post in db */
app.put('/api/v1/posts/:id', async (req, res) => {
  const affectedRows = await updatePost({ id: req.params.id, ...req.body });
  console.log(affectedRows);
  const post = await getPostById(req.params.id);
  res.status(200).json(post);
});

/** delete existing post */
app.delete('/api/v1/posts/:id', async (req, res) => {
  const affectedRows = await deletePost(req.params.id);
  console.log(affectedRows);
  res.status(204).json(null);
});

app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../client/build/index.html'));
});
