const { getDbPool } = require('./database');

/**
 * Get, paginate and filter posts
 * @param {object} params
 */
async function getPosts({ page, limit, fullName, title, content }) {
  return new Promise((resolve, reject) => {
    const queryParams = [];
    let sql =
      'SELECT p.id as id, title, content, u.full_name as fullName, u.id as userId FROM posts as p INNER JOIN users as u ON u.id=p.user_id  WHERE 1 ';
    let countSql =
      'SELECT COUNT(p.id) as total FROM posts as p INNER JOIN users as u ON u.id=p.user_id  WHERE 1 ';

    if (fullName) {
      sql += 'AND LOWER(full_name) like ? ';
      countSql += 'AND LOWER(full_name) like ? ';
      queryParams.push(`%${fullName.toLowerCase()}%`);
    }

    if (title) {
      sql += 'AND LOWER(title) like ?';
      countSql += 'AND LOWER(title) like ?';
      queryParams.push(`%${title.toLowerCase()}%`);
    }

    if (content) {
      sql += 'AND LOWER(content) like ?';
      countSql += 'AND LOWER(content) like ?';
      queryParams.push(`%${content.toLowerCase()}%`);
    }

    sql += 'LIMIT ? OFFSET ?';
    queryParams.push(limit, (page - 1) * limit);

    getDbPool().query(
      `${sql}; ${countSql}`,
      [...queryParams, ...queryParams],
      (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve([result[0], result[1][0].total]);
        }
      }
    );
  });
}

/**
 * Get single post by id
 * @param {number} id
 */
async function getPostById(id) {
  return new Promise((resolve, reject) => {
    const sql = 'SELECT id, title, content, user_id FROM posts  WHERE id=?';
    getDbPool().query(sql, [id], (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result[0]);
      }
    });
  });
}

/**
 * Create new post in db
 * @param {object} fields
 */
async function createPost({ title, content, userId }) {
  return new Promise((resolve, reject) => {
    const sql = 'INSERT INTO posts (title, content, user_id) VALUES ?';
    const values = [[title, content, userId]];
    getDbPool().query(sql, [values], (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result.insertId);
      }
    });
  });
}

/**
 * Update post in db
 * @param {object} fields
 */
async function updatePost({ id, title, content, userId }) {
  return new Promise((resolve, reject) => {
    const sql = 'UPDATE posts SET title=?, content=?, user_id=? WHERE id=?';
    getDbPool().query(sql, [title, content, userId, id], (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result.affectedRows);
      }
    });
  });
}
/**
 * Delete post from db
 * @param {number} id
 */
async function deletePost(id) {
  return new Promise((resolve, reject) => {
    const sql = 'DELETE FROM posts WHERE id=?';
    getDbPool().query(sql, [id], (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result.affectedRows);
      }
    });
  });
}

module.exports = {
  getPosts,
  getPostById,
  createPost,
  updatePost,
  deletePost,
};
