const mysql = require('mysql');

/** cache db pool for later use */
let cachedDbPool;
/** create or return previusly created db pool */
function getDbPool() {
  if (!cachedDbPool) {
    cachedDbPool = mysql.createPool({
      connectionLimit: 1,
      host: 'eu-cdbr-west-03.cleardb.net',
      user: 'b2c73ff2370581',
      password: '0e53f284',
      database: 'heroku_e9a480c903bc9b3',
      multipleStatements: true,
    });
  }
  return cachedDbPool;
}

module.exports = {
  getDbPool,
};
