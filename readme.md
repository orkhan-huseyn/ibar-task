# IBA Task User and Posts

Take a look at application: [https://iba-users.herokuapp.com/](https://iba-users.herokuapp.com/)
Front-end Library: [Chakra-UI](https://chakra-ui.com/)

Stack: NodeJs (with Express), MySQL, React

## Prerequisites

To run this application you must have Nodejs v12 or higher installed on your machine.

## Instructions

### Development

To run it locally you can use `yarn dev` command. It will start React application in development mode and start the backend server.
In this case front-end and back-end will be seperate applications running on different origins.

Then on your browser, go to `http://localhost:3000` to take a look at the app.

### Production build

For running production build of React application, use `yarn prod`. It will build React application and start the backend server. In this case built static files will be served from NodeJs application (monolith).

### Docker build

Use `docker build -t iba-users:latest .` to build Docker image then use `docker run -p 80:80 iba-users:latest` to run the container.

## Details

### Lighthouse test results

Take a look at it on [Google PageSpeed Insights](https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fiba-users.herokuapp.com%2F&tab=desktop).

- Performance: 78% on Mobile, 99% on Desktop
- Accessibility: 100%
- SEO: 100%
- Best Practices: 93%

### Possible improvement

- Better folder structure on backend (if it gets larger, of couse)
- Enable GZIP compression for smaller file transfers over network
- Avoid writing credentials (in this case DB credentials ) inside source code. Use enviroment variables inside CI/CD tool.
- Make front-end mobile friendly
- Use better design :)
- Write unit (integration, snapshot) tests
