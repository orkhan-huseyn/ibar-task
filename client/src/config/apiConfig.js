const apiConfig = Object.freeze({
  development: {
    baseUrl: 'http://localhost/api/v1/',
    defaultHeaders: {
      'Content-Type': 'application/json',
    },
  },
  production: {
    baseUrl: '/api/v1/',
    defaultHeaders: {
      'Content-Type': 'application/json',
    },
  },
});

export default apiConfig[process.env.NODE_ENV];
