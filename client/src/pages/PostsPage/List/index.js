import React, { useState } from 'react';
import {
  IconButton,
  Button,
  Spinner,
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
} from '@chakra-ui/core';

export default function PostList({
  posts,
  isFetching,
  editPost,
  deletePost,
  offset,
}) {
  const [isLoading, setIsLoading] = useState(false);
  const [isOpen, setIsOpen] = useState();
  const [postId, setPostId] = useState();

  const onClose = () => setIsOpen(false);
  const cancelRef = React.useRef();

  function onDeleteClick(postId) {
    setIsOpen(true);
    setPostId(postId);
  }

  function onConfirmDeletion() {
    setIsLoading(true);
    deletePost(postId, function () {
      setIsLoading(false);
      onClose();
    });
  }

  return (
    <>
      <AlertDialog
        isOpen={isOpen}
        leastDestructiveRef={cancelRef}
        onClose={onClose}
      >
        <AlertDialogOverlay />
        <AlertDialogContent>
          <AlertDialogHeader fontSize="lg" fontWeight="bold">
            Delete post
          </AlertDialogHeader>

          <AlertDialogBody>
            Are you sure? You can't undo this action afterwards.
          </AlertDialogBody>

          <AlertDialogFooter>
            <Button ref={cancelRef} onClick={onClose}>
              Cancel
            </Button>
            <Button
              isLoading={isLoading}
              variantColor="red"
              onClick={onConfirmDeletion}
              ml={3}
            >
              Delete
            </Button>
          </AlertDialogFooter>
        </AlertDialogContent>
      </AlertDialog>

      <table summary="List, filter and paginate users">
        <thead>
          <tr>
            <th>#</th>
            <th>Title</th>
            <th>Content</th>
            <th>User</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {posts.length === 0 && !isFetching ? (
            <tr>
              <td style={{ textAlign: 'center' }} colSpan={5}>
                No posts found
              </td>
            </tr>
          ) : null}
          {isFetching ? (
            <tr>
              <td style={{ textAlign: 'center' }} colSpan={5}>
                <Spinner />
              </td>
            </tr>
          ) : (
            posts.map((post, i) => (
              <tr key={post.id}>
                <td>{offset + i + 1}</td>
                <td>{post.title}</td>
                <td>{post.content}</td>
                <td>{post.fullName}</td>
                <td>
                  <IconButton
                    onClick={() => editPost(post)}
                    style={{ marginRight: '5px' }}
                    aria-label="Edit user"
                    icon="edit"
                  />
                  <IconButton
                    onClick={() => onDeleteClick(post.id)}
                    aria-label="Delete user"
                    icon="delete"
                  />
                </td>
              </tr>
            ))
          )}
        </tbody>
      </table>
    </>
  );
}
