import React from 'react';

import {
  FormControl,
  FormLabel,
  FormErrorMessage,
  Input,
  Textarea,
  Select,
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
} from '@chakra-ui/core';

import { Formik } from 'formik';

export default function PostForm({
  initialRef,
  initialFormValues,
  isOpen,
  onClose,
  users,
  insertPost,
  updatePost,
}) {
  return (
    <Modal initialFocusRef={initialRef} isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Create new post</ModalHeader>
        <ModalCloseButton />

        <Formik
          initialValues={initialFormValues}
          validate={(values) => {
            const errors = {};
            if (!values.userId) {
              errors.userId = 'User is required';
            }
            if (!values.title) {
              errors.title = 'Title is required';
            }
            if (!values.content) {
              errors.content = 'Content is required';
            }
            return errors;
          }}
          onSubmit={(values, actions) => {
            if (values.id) {
              updatePost(values, function () {
                actions.setSubmitting(false);
              });
            } else {
              insertPost(values, function () {
                actions.setSubmitting(false);
              });
            }
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
          }) => (
            <>
              <ModalBody pb={6}>
                <form id="userForm" onSubmit={handleSubmit}>
                  <FormControl
                    isInvalid={errors.title && touched.title}
                    isRequired
                  >
                    <FormLabel htmlFor="title">Title</FormLabel>
                    <Input
                      type="text"
                      id="title"
                      name="title"
                      placeholder="Post title"
                      ref={initialRef}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.title}
                    />
                    <FormErrorMessage>{errors.title}</FormErrorMessage>
                  </FormControl>

                  <FormControl
                    isInvalid={errors.content && touched.content}
                    isRequired
                  >
                    <FormLabel htmlFor="content">Content</FormLabel>
                    <Textarea
                      id="content"
                      name="content"
                      placeholder="Post content"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.content}
                    />
                    <FormErrorMessage>{errors.content}</FormErrorMessage>
                  </FormControl>

                  <FormControl
                    isInvalid={errors.userId && touched.userId}
                    isRequired
                  >
                    <FormLabel htmlFor="userId">User</FormLabel>
                    <Select
                      id="userId"
                      name="userId"
                      placeholder="Select user"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.userId}
                    >
                      {users.map((user) => (
                        <option key={user.id} value={user.id}>
                          {user.fullName}
                        </option>
                      ))}
                    </Select>
                    <FormErrorMessage>{errors.userId}</FormErrorMessage>
                  </FormControl>
                </form>
              </ModalBody>
              <ModalFooter>
                <Button
                  type="submit"
                  form="userForm"
                  isLoading={isSubmitting}
                  variantColor="blue"
                  mr={3}
                >
                  Save
                </Button>
                <Button onClick={onClose}>Cancel</Button>
              </ModalFooter>
            </>
          )}
        </Formik>
      </ModalContent>
    </Modal>
  );
}
