import React from 'react';

const PostsPageLazy = React.lazy(() => import('./index'));

export default () => (
  <React.Suspense fallback={<div>Loading component...</div>}>
    <PostsPageLazy />
  </React.Suspense>
);
