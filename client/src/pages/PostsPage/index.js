import React, { useState, useEffect } from 'react';
import ReactPaginate from 'react-paginate';
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  Button,
  useDisclosure,
  Input,
} from '@chakra-ui/core';

import API from 'api';
import PostList from './List';
import PostForm from './Form';

import './styles.scss';

const perPage = 5;
let insertPost$, updatePost$;

function PostsPage() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const initialRef = React.useRef();

  const [refetch, setRefetch] = useState(false);
  const [posts, setPosts] = useState([]);
  const [users, setUsers] = useState([]);

  const [currentPage, setCurrentPage] = useState(1);
  const [filters, setFilters] = useState({
    title: '',
    content: '',
    fullName: '',
  });
  const [isFetching, setFetchingStatus] = useState(false);
  const [initialFormValues, setInitialFormValues] = useState({
    title: '',
    content: '',
    userId: '',
  });

  const [pageCount, setPageCount] = useState(0);
  const [offset, setOffset] = useState(0);

  useEffect(() => {
    const fetchUsers$ = API.getAllUsers().subscribe(({ response, status }) => {
      if (status === 200) {
        setUsers(response);
      }
    });

    return () => {
      fetchUsers$ && fetchUsers$.unsubscribe();
    };
  }, []);

  useEffect(() => {
    setFetchingStatus(true);

    let params = {
      page: currentPage,
      limit: perPage,
    };

    if (filters.title) {
      params.title = filters.title;
    }

    if (filters.content) {
      params.content = filters.content;
    }

    if (filters.fullName) {
      params.fullName = filters.fullName;
    }

    let fetchPosts$ = API.getPosts(params).subscribe(({ response, status }) => {
      if (status === 200) {
        setPosts(response.posts);
        setPageCount(Math.ceil(response.total / perPage));
        setOffset((currentPage - 1) * perPage);
      }
      setFetchingStatus(false);
    });

    return () => {
      fetchPosts$ && fetchPosts$.unsubscribe();
      insertPost$ && insertPost$.unsubscribe();
      updatePost$ && updatePost$.unsubscribe();
    };
  }, [currentPage, refetch, filters]);

  function insertPost(values, callback) {
    insertPost$ = API.createPost(values).subscribe(({ status }) => {
      if (status === 201) {
        setRefetch((refetch) => !refetch);
        callback();
        onClose();
      }
    });
  }

  function updatePost(values, callback) {
    updatePost$ = API.updatePost(values).subscribe(({ status }) => {
      if (status === 200) {
        setRefetch((refetch) => !refetch);
        callback();
        onClose();
      }
    });
  }

  function onOpenModal(post) {
    if (post) {
      setInitialFormValues(post);
    } else {
      setInitialFormValues({ title: '', content: '', userId: '' });
    }
    onOpen();
  }

  function onDeletePost(id, callback) {
    API.deletePost(id).subscribe(({ status }) => {
      if (status === 204) {
        setRefetch((refetch) => !refetch);
        callback();
      }
    });
  }

  function onPageChange(currPage) {
    setCurrentPage(currPage.selected + 1);
  }

  function onFilterKeydown(e) {
    const isReturnKey = e.key === 'Enter';
    if (isReturnKey) {
      setFilters({
        ...filters,
        [e.target.name]: e.target.value,
      });
    }
  }

  return (
    <div className="posts">
      <div className="posts__header">
        <Breadcrumb>
          <BreadcrumbItem>
            <BreadcrumbLink href="#">Home</BreadcrumbLink>
          </BreadcrumbItem>

          <BreadcrumbItem isCurrentPage>
            <BreadcrumbLink href="#">Posts</BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumb>

        <Button
          onClick={() => onOpenModal(null)}
          size="sm"
          variantColor="teal"
          leftIcon="small-add"
        >
          Add Post
        </Button>
      </div>

      <div className="posts__filters">
        <Input
          onKeyDown={onFilterKeydown}
          size="sm"
          name="title"
          style={{ width: '300px', marginRight: '20px' }}
          aria-label="Search post by title"
          placeholder="Search post by title"
        />

        <Input
          onKeyDown={onFilterKeydown}
          size="sm"
          name="content"
          style={{ width: '300px', marginRight: '20px' }}
          aria-label="Search post by content"
          placeholder="Search post by content"
        />

        <Input
          onKeyDown={onFilterKeydown}
          size="sm"
          name="fullName"
          style={{ width: '300px' }}
          aria-label="Search post by user's fullname"
          placeholder="Search post by user's fullname"
        />
      </div>

      <PostForm
        users={users}
        initialFormValues={initialFormValues}
        insertPost={insertPost}
        updatePost={updatePost}
        initialRef={initialRef}
        isOpen={isOpen}
        onClose={onClose}
      />

      <div className="posts__list">
        <PostList
          offset={offset}
          posts={posts}
          isFetching={isFetching}
          deletePost={onDeletePost}
          editPost={onOpenModal}
        />
      </div>

      <div className="posts__footer">
        <ReactPaginate
          breakLabel="..."
          breakClassName="break"
          pageCount={pageCount}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={onPageChange}
          containerClassName="pagination"
          subContainerClassName="pages pagination"
          activeClassName="active"
        />
      </div>
    </div>
  );
}

export default PostsPage;
