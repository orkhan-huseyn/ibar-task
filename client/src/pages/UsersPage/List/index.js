import React, { useState } from 'react';
import {
  IconButton,
  Button,
  Spinner,
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
} from '@chakra-ui/core';

export default function UserList({
  users,
  isFetching,
  editUser,
  deleteUser,
  offset,
}) {
  const [isLoading, setIsLoading] = useState(false);
  const [isOpen, setIsOpen] = useState();
  const [userId, setUserId] = useState();

  const onClose = () => setIsOpen(false);
  const cancelRef = React.useRef();

  function onDeleteClick(userId) {
    setIsOpen(true);
    setUserId(userId);
  }

  function onConfirmDeletion() {
    setIsLoading(true);
    deleteUser(userId, function () {
      setIsLoading(false);
      onClose();
    });
  }

  return (
    <>
      <AlertDialog
        isOpen={isOpen}
        leastDestructiveRef={cancelRef}
        onClose={onClose}
      >
        <AlertDialogOverlay />
        <AlertDialogContent>
          <AlertDialogHeader fontSize="lg" fontWeight="bold">
            Delete user
          </AlertDialogHeader>

          <AlertDialogBody>
            Are you sure? You can't undo this action afterwards.
          </AlertDialogBody>

          <AlertDialogFooter>
            <Button ref={cancelRef} onClick={onClose}>
              Cancel
            </Button>
            <Button
              isLoading={isLoading}
              variantColor="red"
              onClick={onConfirmDeletion}
              ml={3}
            >
              Delete
            </Button>
          </AlertDialogFooter>
        </AlertDialogContent>
      </AlertDialog>

      <table summary="List, filter and paginate users">
        <thead>
          <tr>
            <th>#</th>
            <th>Full Name</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {users.length === 0 && !isFetching ? (
            <tr>
              <td style={{ textAlign: 'center' }} colSpan={3}>
                No users found
              </td>
            </tr>
          ) : null}
          {isFetching ? (
            <tr>
              <td style={{ textAlign: 'center' }} colSpan={3}>
                <Spinner />
              </td>
            </tr>
          ) : (
            users.map((user, i) => (
              <tr key={user.id}>
                <td>{offset + i + 1}</td>
                <td>{user.fullName}</td>
                <td>
                  <IconButton
                    onClick={() => editUser(user)}
                    style={{ marginRight: '5px' }}
                    aria-label="Edit user"
                    icon="edit"
                  />
                  <IconButton
                    onClick={() => onDeleteClick(user.id)}
                    aria-label="Delete user"
                    icon="delete"
                  />
                </td>
              </tr>
            ))
          )}
        </tbody>
      </table>
    </>
  );
}
