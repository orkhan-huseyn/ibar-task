import React from 'react';

import {
  FormControl,
  FormLabel,
  FormErrorMessage,
  Input,
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
} from '@chakra-ui/core';

import { Formik } from 'formik';

export default function UserForm({
  initialRef,
  initialFormValues,
  isOpen,
  onClose,
  insertUser,
  updateUser,
}) {
  return (
    <Modal initialFocusRef={initialRef} isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Create new user</ModalHeader>
        <ModalCloseButton />

        <Formik
          initialValues={initialFormValues}
          validate={(values) => {
            const errors = {};
            if (!values.fullName) {
              errors.fullName = 'Name is required';
            }
            return errors;
          }}
          onSubmit={(values, actions) => {
            if (values.id) {
              updateUser(values, function () {
                actions.setSubmitting(false);
              });
            } else {
              insertUser(values, function () {
                actions.setSubmitting(false);
              });
            }
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
          }) => (
            <>
              <ModalBody pb={6}>
                <form id="userForm" onSubmit={handleSubmit}>
                  <FormControl
                    isInvalid={errors.fullName && touched.fullName}
                    isRequired
                  >
                    <FormLabel htmlFor="fullName">Full name</FormLabel>
                    <Input
                      type="text"
                      id="fullName"
                      name="fullName"
                      placeholder="Full name"
                      ref={initialRef}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.fullName}
                    />
                    <FormErrorMessage>{errors.fullName}</FormErrorMessage>
                  </FormControl>
                </form>
              </ModalBody>
              <ModalFooter>
                <Button
                  type="submit"
                  form="userForm"
                  isLoading={isSubmitting}
                  variantColor="blue"
                  mr={3}
                >
                  Save
                </Button>
                <Button onClick={onClose}>Cancel</Button>
              </ModalFooter>
            </>
          )}
        </Formik>
      </ModalContent>
    </Modal>
  );
}
