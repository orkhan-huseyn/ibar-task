import React from 'react';

const UsersPageLazy = React.lazy(() => import('./index'));

export default () => (
  <React.Suspense fallback={<div>Loading component...</div>}>
    <UsersPageLazy />
  </React.Suspense>
);
