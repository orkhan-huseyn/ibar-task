import React, { useState, useEffect } from 'react';
import ReactPaginate from 'react-paginate';
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  Button,
  useDisclosure,
  Input,
} from '@chakra-ui/core';

import API from 'api';
import UserList from './List';
import UserForm from './Form';

import './styles.scss';

const perPage = 5;
let insertUser$, updateUser$;

function UsersPage() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const initialRef = React.useRef();

  const [refetch, setRefetch] = useState(false);
  const [users, setUsers] = useState([]);

  const [currentPage, setCurrentPage] = useState(1);
  const [filters, setFilters] = useState({ fullName: '' });
  const [isFetching, setFetchingStatus] = useState(false);
  const [initialFormValues, setInitialFormValues] = useState({ fullName: '' });

  const [pageCount, setPageCount] = useState(0);
  const [offset, setOffset] = useState(0);

  useEffect(() => {
    setFetchingStatus(true);

    let params = {
      page: currentPage,
      limit: perPage,
    };

    if (filters.fullName) {
      params.fullName = filters.fullName;
    }

    let fetchUsers$ = API.getUsers(params).subscribe(({ response, status }) => {
      if (status === 200) {
        setUsers(response.users);
        setPageCount(Math.ceil(response.total / perPage));
        setOffset((currentPage - 1) * perPage);
      }
      setFetchingStatus(false);
    });

    return () => {
      fetchUsers$ && fetchUsers$.unsubscribe();
      insertUser$ && insertUser$.unsubscribe();
      updateUser$ && updateUser$.unsubscribe();
    };
  }, [currentPage, refetch, filters]);

  function insertUser(values, callback) {
    insertUser$ = API.createUser(values).subscribe(({ status }) => {
      if (status === 201) {
        setRefetch((refetch) => !refetch);
        setCurrentPage(1);
        callback();
        onClose();
      }
    });
  }

  function updateUser(values, callback) {
    updateUser$ = API.updateUser(values).subscribe(({ status }) => {
      if (status === 200) {
        setRefetch((refetch) => !refetch);
        setCurrentPage(1);
        callback();
        onClose();
      }
    });
  }

  function onOpenModal(user) {
    if (user) {
      setInitialFormValues(user);
    } else {
      setInitialFormValues({ fullName: '' });
    }
    onOpen();
  }

  function onDeleteUser(id, callback) {
    API.deleteUser(id).subscribe(({ status }) => {
      if (status === 204) {
        setRefetch((refetch) => !refetch);
        setCurrentPage(1);
        callback();
      }
    });
  }

  function onPageChange(currPage) {
    setCurrentPage(currPage.selected + 1);
  }

  function onFilterKeydown(e) {
    const isReturnKey = e.key === 'Enter';
    if (isReturnKey) {
      setFilters({
        ...filters,
        [e.target.name]: e.target.value,
      });
    }
  }

  return (
    <div className="users">
      <div className="users__header">
        <Breadcrumb>
          <BreadcrumbItem>
            <BreadcrumbLink href="#">Home</BreadcrumbLink>
          </BreadcrumbItem>

          <BreadcrumbItem isCurrentPage>
            <BreadcrumbLink href="#">Users</BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumb>

        <Button
          onClick={() => onOpenModal(null)}
          size="sm"
          variantColor="teal"
          leftIcon="small-add"
        >
          Add User
        </Button>
      </div>

      <div className="users__filters">
        <Input
          onKeyDown={onFilterKeydown}
          size="sm"
          name="fullName"
          style={{ width: '300px' }}
          aria-label="Search user by fullname"
          placeholder="Search user by fullname"
        />
      </div>

      <UserForm
        initialFormValues={initialFormValues}
        insertUser={insertUser}
        updateUser={updateUser}
        initialRef={initialRef}
        isOpen={isOpen}
        onClose={onClose}
      />

      <div className="users__list">
        <UserList
          offset={offset}
          users={users}
          isFetching={isFetching}
          deleteUser={onDeleteUser}
          editUser={onOpenModal}
        />
      </div>

      <div className="users__footer">
        <ReactPaginate
          breakLabel="..."
          breakClassName="break"
          pageCount={pageCount}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={onPageChange}
          containerClassName="pagination"
          subContainerClassName="pages pagination"
          activeClassName="active"
        />
      </div>
    </div>
  );
}

export default UsersPage;
