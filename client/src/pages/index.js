import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Layout from 'components/Layout';
import UsersPage from './UsersPage/loadable';
import PostsPage from './PostsPage/loadable';

import './styles.scss';

function App() {
  return (
    <Layout>
      <Switch>
        <Route path="/users" component={UsersPage} />
        <Route path="/posts" component={PostsPage} />
        <Route
          render={() => {
            return <Redirect to="/users" />;
          }}
        />
      </Switch>
    </Layout>
  );
}

export default App;
