import React from 'react';
import './styles.scss';

function SkipLink() {
  return (
    <div id="skip-link">
      <a
        href="#main-content"
        title="Skip to main content"
        className="element-invisible element-focusable"
      >
        Skip to main content
      </a>
    </div>
  );
}

export default SkipLink;
