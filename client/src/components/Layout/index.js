import React from 'react';

import Header from './Header';
import Navigation from './Navigation';
import SkipLink from 'components/SkipLink';

import './styles.scss';

function Layout({ children }) {
  return (
    <>
      <SkipLink />
      <div className="layout">
        <Header />
        <Navigation />
        <main id="main-content" className="layout__content">
          {children}
        </main>
      </div>
    </>
  );
}

export default Layout;
