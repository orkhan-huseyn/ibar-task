import React from 'react';
import { NavLink } from 'react-router-dom';
import { Link } from '@chakra-ui/core';

import './styles.scss';

const pages = [
  {
    title: 'Users',
    path: '/users',
  },
  {
    title: 'Posts',
    path: '/posts',
  },
];

function Navigation() {
  return (
    <div className="navigation">
      <nav className="navigation__nav">
        {pages.map((page) => (
          <Link
            as={NavLink}
            className="navigation__nav__item"
            key={page.path}
            title={page.title}
            to={page.path}
          >
            {page.title}
          </Link>
        ))}
      </nav>
    </div>
  );
}

export default Navigation;
