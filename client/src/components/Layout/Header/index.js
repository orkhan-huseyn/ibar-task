import React from 'react';
import { NavLink } from 'react-router-dom';
import {
  useColorMode,
  IconButton,
  Menu,
  MenuButton,
  Button,
  MenuItem,
  MenuList,
  Link,
} from '@chakra-ui/core';

import logo from 'assets/logo.svg';
import logoWhite from 'assets/logo-w.svg';

import './styles.scss';

function Header() {
  const { colorMode, toggleColorMode } = useColorMode();
  return (
    <header className="header">
      <Link as={NavLink} to="/" className="header__logo">
        <h1 className="sr-only">International Bank of Azerbaijan Task Users</h1>
        <img src={colorMode === 'light' ? logo : logoWhite} alt="ibar logo" />
      </Link>
      <div className="header__avatar">
        <Menu>
          <MenuButton as={Button} rightIcon="chevron-down">
            Orkhan Huseynli
          </MenuButton>
          <MenuList>
            <MenuItem>Profile</MenuItem>
            <MenuItem as="a" href="#">
              Logout
            </MenuItem>
          </MenuList>
        </Menu>
      </div>
      <IconButton
        aria-label={
          colorMode === 'light' ? 'switch to dark mode' : 'switch to light mode'
        }
        icon={colorMode === 'light' ? 'moon' : 'sun'}
        onClick={toggleColorMode}
      />
    </header>
  );
}

export default Header;
