import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import App from './pages';

import {
  CSSReset,
  ColorModeProvider,
  ThemeProvider,
  theme,
} from '@chakra-ui/core';

import './index.scss';

ReactDOM.render(
  <BrowserRouter>
    <ThemeProvider theme={theme}>
      <ColorModeProvider>
        <CSSReset />
        <App />
      </ColorModeProvider>
    </ThemeProvider>
  </BrowserRouter>,
  document.getElementById('root')
);
