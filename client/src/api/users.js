import { of } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { catchError } from 'rxjs/operators';
import quertString from 'query-string';

import apiConfig from 'config/apiConfig';

export function getAllUsers() {
  return ajax({
    method: 'GET',
    url: `${apiConfig.baseUrl}users/all`,
    headers: apiConfig.defaultHeaders,
  }).pipe(
    catchError((error) => {
      return of(error);
    })
  );
}

export function getUsers(params) {
  const urlParams = quertString.stringify(params);
  return ajax({
    method: 'GET',
    url: `${apiConfig.baseUrl}users?${urlParams}`,
    headers: apiConfig.defaultHeaders,
  }).pipe(
    catchError((error) => {
      return of(error);
    })
  );
}

export function getUserById(id) {
  return ajax({
    method: 'GET',
    url: `${apiConfig.baseUrl}users/${id}`,
    headers: apiConfig.defaultHeaders,
  }).pipe(
    catchError((error) => {
      return of(error);
    })
  );
}

export function createUser(requestBody) {
  return ajax({
    method: 'POST',
    url: `${apiConfig.baseUrl}users`,
    headers: apiConfig.defaultHeaders,
    body: requestBody,
  }).pipe(
    catchError((error) => {
      return of(error);
    })
  );
}

export function updateUser(requestBody) {
  return ajax({
    method: 'PUT',
    url: `${apiConfig.baseUrl}users/${requestBody.id}`,
    headers: apiConfig.defaultHeaders,
    body: requestBody,
  }).pipe(
    catchError((error) => {
      return of(error);
    })
  );
}

export function deleteUser(id) {
  return ajax({
    method: 'DELETE',
    url: `${apiConfig.baseUrl}users/${id}`,
    headers: apiConfig.defaultHeaders,
  }).pipe(
    catchError((error) => {
      return of(error);
    })
  );
}
