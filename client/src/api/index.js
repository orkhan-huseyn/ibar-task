import * as postServices from './posts';
import * as userServices from './users';

export default {
  ...postServices,
  ...userServices,
};
