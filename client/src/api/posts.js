import { of } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { catchError } from 'rxjs/operators';
import quertString from 'query-string';

import apiConfig from 'config/apiConfig';

export function getPosts(params) {
  const urlParams = quertString.stringify(params);
  return ajax({
    method: 'GET',
    url: `${apiConfig.baseUrl}posts?${urlParams}`,
    headers: apiConfig.defaultHeaders,
  }).pipe(
    catchError((error) => {
      return of(error);
    })
  );
}

export function getPostById(id) {
  return ajax({
    method: 'GET',
    url: `${apiConfig.baseUrl}posts/${id}`,
    headers: apiConfig.defaultHeaders,
  }).pipe(
    catchError((error) => {
      return of(error);
    })
  );
}

export function createPost(requestBody) {
  return ajax({
    method: 'POST',
    url: `${apiConfig.baseUrl}posts`,
    headers: apiConfig.defaultHeaders,
    body: requestBody,
  }).pipe(
    catchError((error) => {
      return of(error);
    })
  );
}

export function updatePost(requestBody) {
  return ajax({
    method: 'PUT',
    url: `${apiConfig.baseUrl}posts/${requestBody.id}`,
    headers: apiConfig.defaultHeaders,
    body: requestBody,
  }).pipe(
    catchError((error) => {
      return of(error);
    })
  );
}

export function deletePost(id) {
  return ajax({
    method: 'DELETE',
    url: `${apiConfig.baseUrl}posts/${id}`,
    headers: apiConfig.defaultHeaders,
  }).pipe(
    catchError((error) => {
      return of(error);
    })
  );
}
